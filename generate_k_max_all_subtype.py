
def get_most_frequent_k(l):
    dict_vals = {}
    
    for v in l:
        
        if v not in dict_vals:
            dict_vals[v] = 1
        else:
            dict_vals[v] += 1
    print s,dict_vals
    of.write(str(s) + '\t' + str(dict_vals) + '\n')
    return max(dict_vals, key=dict_vals.get)
        

if __name__ == '__main__':
    
    inpath_pre = '../final_results_v2/tcga6_s_18_36/'
        
    cancer_type_list = [ 'AML', 'BIC','Colon', 'GBM', 'Kidney', 'LIHC', 'LUSC', 'OV', 'SARC', 'SKCM']
    k = 8
    s_start = 18
    s_end = 36
    step = 2
    
    outfile = inpath_pre + 'all_subtype_maxfile.txt'
    outfile2 = inpath_pre + 'all_subtype_k_distribution.txt'

    of = open(outfile2, 'w')

    d = {c: {} for c in cancer_type_list}
    
    for cancer_type in cancer_type_list:
        
        d_temp = {}
        print cancer_type
        of.write(cancer_type + '\n')
        inpath  = inpath_pre + cancer_type + '_k' + str(k) + '_s' + str(s_end) + '_clustering_results_single_run_1/'
        
        infile = inpath + cancer_type + '_maxfile.txt'
        
        with open(infile, 'r') as f:
            
            for line in f.readlines():
                
                line = line.split()
                
                s = int(line[0])
                
                
                if s not in d_temp:
                    d_temp[s] = []
                
                if len(d_temp[s])<100:
                    d_temp[s].append(int(line[2]))
                    
        for s in sorted(d_temp):
            d[cancer_type][s] = get_most_frequent_k(d_temp[s])
        of.write('\n')
            


    with open(outfile, 'w') as f:
        
        f.write('sparsity')
        for cancer_type in cancer_type_list:
            f.write('\t' + cancer_type)
        f.write('\n')
        
        for s in range(s_start,s_end+1, step):
            f.write(str(s))
            
            for cancer_type in cancer_type_list:
                try:
                    f.write('\t' + str(d[cancer_type][s]))
                except:
                    f.write('\t' + ' ')
            f.write('\n')
                    
    # with open(outfile2, 'w') as f:
    #
    #     for cancer_type in cancer_type_list:
    #         f.write(cancer_type + '\n')
    #
    #         for s in sorted(d[cancer_type]):
    #             f.write(str(s) + '\t')
    #             f.write(str(d[cancer_type][s]) + '\n')
    #         f.write('\n')