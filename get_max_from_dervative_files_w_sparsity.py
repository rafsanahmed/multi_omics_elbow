from collections import OrderedDict
import matplotlib.pyplot as plt
import glob
import os

inpath = '../final_results_v2/tcga/clustering_results_single_run_9/'
of = open(inpath + 'kidney_maxfile1.txt', 'w')
temp = sorted(glob.glob(inpath+'second_derivative_*'))

f_list = [os.path.basename(f) for f in temp]
print len(f_list)
count = 0
for ff in f_list:
    c = ff.split('_')[-1][:-3]
    with open(inpath + ff, 'r') as f:
        d = {}
        lines = f.readlines()
        for line in lines:
            line = line.split()
            try:
                d[line[0][1:]] = float(line[2])
            except:
                d[line[0][1:]] = 0

        print d
        print max(d, key=d.get), max(d.values())
        print ''

        of.write(ff.split('_.')[-1] + '\t' +  str(max(d, key=d.get)) + '\t' + str(max(d.values())) + '\n')



# d = {}
# with open(ff,'r') as f:
#     lines = f.readlines()
#     for line in lines:
#         line = line.split()
#         if line[1] in d:
#             d[line[1]] += 1
#         else:
#             d[line[1]] = 1
#
# print d
# print 'k' + str(max(d,key=d.get)), max(d.values())
#
# x = []
# y = []
#
# for k in sorted(d):
#     x.append(k)
#     y.append(d[k])
#
# plt.plot(x,y)
# plt.xlabel('K')
# plt.ylabel('#times chosen')
# plt.show()
