import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import math
from tqdm import tqdm, trange
from sklearn.preprocessing import normalize


def normalize_row(data):
    temp = data-np.mean(data)
    k = np.std(temp)
    if k!= 0:
        return temp/k
    else:
        return []

def log_and_normalize(infile, outfile, norm = False):

    with open(inpath+cancer_type+'_'+infile+'_df.txt', 'r') as f, open(inpath +cancer_type+'_'+ outfile+'_subset.txt', 'w') as of:

        lines = f.readlines()
        header= lines[0]
        #print 'pre', header
        # header = lines[0].replace('\"', "")
        #if cancer_type == 'OV':
            #header = header.replace('-', '.')
        # header = header.replace(' ', '\t')


        print len(set(header.split(' ')))
        print 'header'
        print header
        of.write('gene_id' + header)

        for line in tqdm(lines[1:]):
            line2 = line.replace('\"','').split()
            line3 = [float(k) for k in line2[1:]]
            # print 'var', np.var(line3)

            #check variance of line
            # if np.var(line3)==0:
            #     print 'var 00000000000', line2[0]
            if np.var(line3)>0 and len(line3) > line3.count(0):
                #print np.var(line3)
                log_line3 = []
                for l in line3:
                    #get log(1+val)
                    log_line3.append(math.log(1+float(l),2))
                #print line3

                if norm == False:
                    out = log_line3
                else:
                    out = normalize_row(log_line3)

                if len(out) != 0:
                    of.write(line2[0].replace('.','|'))
                    for n in out:
                        of.write('\t' + str(n))
                of.write('\n')


def get_patients_data(infile, patients_list):

    # with open(inpath+infile+'.txt', 'r') as f, open(inpath +cancer_type+'_df_'+ outfile+'_subset.txt', 'w') as of:
    print infile
    if infile == 'mirna':
        df_orig = pd.read_table(inpath+infile+'.txt', sep=' ', header=0, index_col=0)
        df_orig = df_orig[patients_list]

    else:
        df_orig = pd.read_table(inpath+infile+'.txt', sep=' ', header=0, index_col=0, skipinitialspace=True, usecols=patients_list)

    print df_orig.head(), df_orig.shape

    df_orig.to_csv(inpath+cancer_type+'_'+infile+'_df.txt', sep='\t')


def get_patients_set(infile):
    with open(inpath+infile+'.txt', 'r') as f:
        header = f.readlines()[0]
        header = header.replace('\"', "").replace(' ', '\t').replace('-', '.')
        print len(set(header.split()))
        return set(header.split())




if __name__ == '__main__':

    cancer_type = 'OV'
    inpath = '../new_datasets/' + cancer_type + '/'
    input_files = ['exp', 'methy', 'mirna']
    output_files = ['gene' , 'methylation', 'mirna']

    list_set_pat = []
    for om in range(len(input_files)):
        print 'len', input_files[om], len(get_patients_set(input_files[om]))
        list_set_pat.append(get_patients_set(input_files[om]))

    patients= set.intersection(*list_set_pat)
    print 'pat',len(patients)
    print sorted(list(patients))[:5]

    for om in range(len(input_files)):
        get_patients_data(input_files[om], sorted(list(patients)))
        log_and_normalize(input_files[om], output_files[om], norm=False)
