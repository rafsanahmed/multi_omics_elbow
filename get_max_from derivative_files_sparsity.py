import glob
import os
from tqdm import tqdm
from collections import OrderedDict

def get_max(input_file):
    
    d = {}
    with open(input_file, 'r') as f:
        
        for line in f.readlines():
            line = line.split()
            if len(line) == 3:
                d[line[0]] = float(line[2])

        #for k in d:
            #print k, d[k]
            
        k_max = max(d, key=d.get)
        v_max = max(d.values())
        
        return (k_max, v_max)
    
def run_value(filename):
    return int(os.path.basename(filename).split('_')[-1][:-4])
            

if __name__ == '__main__':
    
    
        
    cancer_type_list = ['AML','BIC','Colon', 'GBM', 'Kidney', 'LIHC','LUSC', 'OV', 'SARC', 'SKCM']#['Kidney', 'LIHC', 'LUSC', 'AML']
    k = 8
    s_start = 18
    s_end = 36
    step = 2
    
    for cancer_type in cancer_type_list:
        
        print cancer_type
        #
        inpath  = '../final_results_v2/tcga6_s_18_36/' + cancer_type + '_k' + str(k) + '_s' +str(s_end) +  '_clustering_results_single_run_1/'
        out_maxfile = inpath + cancer_type + '_maxfile.txt'
        
        infile = inpath + cancer_type + '_maxfile.txt'
        d = OrderedDict()

        for s in range(s_start, s_end+1, step):
            
            files_list = sorted(glob.glob(inpath + 'second_derivative_wcss_k' + str(k) + '_s' + str(s) + '*.txt'), key=run_value)         
            d[s] = files_list
                
        with open(out_maxfile, 'w') as f:
                
            for key in tqdm(sorted(d)):
                print key, len(d[key])
                for ff in d[key]:

                    run_temp = run_value(ff)
                    
                    k_temp, v_temp = get_max(ff)
                    f.write(str(key) + '\t' + str(run_temp) + '\t' + k_temp[1:] + '\t' + str(v_temp) + '\n')
            print '\n\n\n'
        #print get_max(inpath + 'second_derivative_wcss_k8_s20_2.txt')
        