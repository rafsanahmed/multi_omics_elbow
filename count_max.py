from collections import OrderedDict
import matplotlib.pyplot as plt

inpath = '../final_results_v2/tcga/clustering_results_single_run_k_7/'
ff = inpath + 'kidney_maxfile.txt'
type = 'Kidney'#'Simulated (4 clusters)'

d = {}
with open(ff,'r') as f:
    lines = f.readlines()
    for line in lines:
        line = line.split()
        if line[1] in d:
            d[line[1]] += 1
        else:
            d[line[1]] = 1

print d
print 'k' + str(max(d,key=d.get)), max(d.values())

x = []
y = []

for k in sorted(d):
    x.append(k)
    y.append(d[k])

plt.plot(x,y)
plt.title(type + ' MVKM Run ' + inpath.split('_')[-1][:-1] + ' Cluster Choice Frequency')
plt.xlabel('K')
plt.ylabel('#times chosen')
plt.savefig(inpath+'fig_1.png')
plt.show()
