import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import math
from tqdm import tqdm, trange
from sklearn.preprocessing import normalize
import ast

def get_max_from_dict(d):

    k_max = max(d,key=d.get)
    v_max = max(d.values())

    return (k_max,v_max)

if __name__ == '__main__':

    inpath = '../final_results_v2/tcga6_s_18_36/'
    infile_1 = inpath + 'all_subtype_maxfile.txt'
    infile_2 = inpath + 'all_subtype_k_distribution.txt'
    outfile = inpath + 'k_best_second_best.txt'

    main_dict = {}
    r = range(18,36+2,2)
    df = pd.read_csv(infile_1, sep='\t', index_col=0, header=0)
    d1 = {}
    for col in df.columns:
        temp_d =  df[col].to_dict()
        temp_d = {int(k):int(v) for k, v in temp_d.iteritems()}
        d1[col] = temp_d

    for k,v in d1.iteritems():
        print k,v



    d2 = {}
    with open(infile_2, 'r') as f:
        lines = f.readlines()
        c = ''
        for line in lines:
            # print line
            if '{' not in line:
                l =line.split()
                if len(l)!=0:
                    c = l[0]
                    if c not in d2:
                        d2[c] = {}
            else:
                l = line.split('\t')
                s = ast.literal_eval(l[1])

                d2[c][int(l[0])] = s

    for k,v in sorted(d2.iteritems()):
        print k,v

    print get_max_from_dict(d1['AML'])

    # c = sorted(d1)
    # with open(outfile,'w') as f:
    #
    #     f.write('attr')
    #     for k in c:
    #         f.write('\t' + k)
    #     f.write('\n')



