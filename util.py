from numpy import *
from numpy.random import randn, seed, multivariate_normal,permutation
import numpy.matlib as matlib
from tqdm import tqdm, trange
from sklearn.decomposition import KernelPCA
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import *
from sklearn.preprocessing import *
from sklearn.mixture import BayesianGaussianMixture
import scipy.sparse as sparse
from scipy.sparse import csc_matrix,lil_matrix
from scipy.linalg import eigh
from sklearn import metrics
from sklearn import preprocessing
# the following two lines are only necessary for Hilal's computer
#import ctypes
#ctypes.CDLL('/Users/User/Desktop/all/UAU/Software/mosek/8/tools/platform/osx64x86/bin/libmosekxx8_0.dylib')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.metrics import classification_report,confusion_matrix
from sklearn.metrics.cluster import normalized_mutual_info_score
from sklearn.metrics import auc
import operator
import mosek
import os
import sys
from scipy.stats.mstats import mquantiles
import subprocess
import pandas as pd
import itertools

# If numpy is installed, use that, otherwise use the  
# Mosek's array module. 
try: 
    from numpy import array,zeros,ones 
except ImportError: 
    from mosek.array import array, zeros, ones 

# Since the actual value of Infinity is ignores, we define it solely 
# for symbolic purposes: 
inf = 0.0 

# Define a stream printer to grab output from MOSEK 
def streamprinter(text): 
    sys.stdout.write(text) 
    sys.stdout.flush() 

# We might write everything directly as a script, but it looks nicer 
# to create a function. 
def run_mosek (trace, num_views):
    # Open MOSEK and create an environment and task 
    # Make a MOSEK environment 
    env = mosek.Env () 
    # Attach a printer to the environment 
    env.set_Stream (mosek.streamtype.log, streamprinter) 
    # Create a task 
    task = env.Task() 
    task.set_Stream (mosek.streamtype.log, streamprinter) 
    # Set up and input bounds and linear coefficients 
    bkc   = [ mosek.boundkey.lo]
    blc   = [ 1.0 ] 
    buc   = [ 1.0 ] 

    bkx   = [ mosek.boundkey.lo] * num_views
    blx   = [ 0.0] * num_views
    bux   = [ 1.0] * num_views
    c     = [ 0.0] * num_views
    asub  = [ array([0])] *  num_views
    aval  = [ array([1])] *  num_views

    numvar = len(bkx) 
    numcon = len(bkc) 

    # Append 'numcon' empty constraints. 
    # The constraints will initially have no bounds.   
    task.appendcons(numcon) 

    # Append 'numvar' variables. 
    # The variables will initially be fixed at zero (x=0).  
    task.appendvars(numvar) 

    for j in range(numvar): 
    # Set the linear term c_j in the objective. 
        task.putcj(j,c[j]) 
        # Set the bounds on variable j 
        # blx[j] <= x_j <= bux[j]  
        task.putbound(mosek.accmode.var,j,bkx[j],blx[j],bux[j]) 
        # Input column j of A  
        task.putacol( j,                  # Variable (column) index. 
                  asub[j],            # Row index of non-zeros in column j. 
                  aval[j])            # Non-zero Values of column j.  
    for i in range(numcon): 
        task.putbound(mosek.accmode.con,i,bkc[i],blc[i],buc[i]) 

    # Input the objective sense (minimize/maximize) 
    task.putobjsense(mosek.objsense.maximize) 

    # Set up and input quadratic objective 
    qsubi = [i for i in range(0,num_views)]
    qsubj = [i for i in range(0,num_views)]
    qval  = [ 2*trace[i] for i in range(0,num_views)]
    #print 'qval ', qval

    task.putqobj(qsubi,qsubj,qval) 

    task.putobjsense(mosek.objsense.minimize) 

    # Optimize 
    task.optimize() 
    # Print a summary containing information 
    # about the solution for debugging purposes 
    task.solutionsummary(mosek.streamtype.msg) 

    prosta = task.getprosta(mosek.soltype.itr) 
    solsta = task.getsolsta(mosek.soltype.itr) 

    # Output a solution 
    xx = zeros(numvar, float) 
    task.getxx(mosek.soltype.itr, 
             xx) 

    if solsta == mosek.solsta.optimal or solsta == mosek.solsta.near_optimal: 
        print("Optimal solution: %s" % xx) 
    elif solsta == mosek.solsta.dual_infeas_cer:  
        print("Primal or dual infeasibility.\n") 
    elif solsta == mosek.solsta.prim_infeas_cer: 
        print("Primal or dual infeasibility.\n") 
    elif solsta == mosek.solsta.near_dual_infeas_cer: 
        print("Primal or dual infeasibility.\n") 
    elif  solsta == mosek.solsta.near_prim_infeas_cer: 
        print("Primal or dual infeasibility.\n") 
    elif mosek.solsta.unknown: 
        print("Unknown solution status") 
    else: 
        print("Other solution status") 

    return xx


def distance_Bo_func(K):
    W = (K + transpose(K)) / 2
    W = W - diag(diag(W))
    D = 1 - dot(diag(1./amax(W, axis = 0)), W)
    D = D - diag(diag(D))
    return D

def is_symmetric(arr):
    if all(arr == arr.T):
        return True
    else:
        return False

'''
def norm_avg_pairwise_dist(K):
    print 'kernel within norm_avg_pairwise ', K
    print np.count_nonzero(K)
    avg_pair_distance = 0 
    (num_patients, num_patients) = K.shape
    for i in range(0, num_patients): 
        for j in range(0, num_patients): 
            avg_pair_distance = avg_pair_distance + (K[i,i] - 2 * K[i,j] + K[j,j])
    avg_pair_distance = avg_pair_distance / float(num_patients * num_patients)
    print avg_pair_distance
    return K / float(avg_pair_distance)
'''

def norm(X,norm_type):
    print norm_type
    #Normalisation region
    if norm_type == 'standard' :
        X = preprocessing.scale(X) 	# zero mean unit variant
        print X[1,1] 
    elif norm_type == 'scale01' : # [0,1]
        scaler = preprocessing.MinMaxScaler((0,1))
        X = scaler.fit_transform(X)

    elif norm_type == 'scale-11' : # [-1,1]
        scaler = preprocessing.MinMaxScaler((-1,1))
        X = scaler.fit_transform(X)    	

    print (X.shape)
    return X

def kernel(X,g):
    K = rbf_kernel(X,gamma = g)
    K = around(K,8)
    print(K.shape)
    if not is_symmetric(K):
        print ' PROBLEMMMM K is not symmetric'
    #K_temp = K
    # K.shape = (X.shape[0]**2,)
    # print('Sigma Value : ', g)
    # print (mquantiles(K,[0,0.5,0.75,0.9,1]))
    return K

def calc_comp_kernel(kernels,z):
    m = len(kernels)  # no. of kernels
    n = kernels[0].shape[0]  # no. of examples
    combined_K = zeros((n, n))
    for i in range(m):
        combined_K += z[i] * z[i] * kernels[i]
    return combined_K

# kernels - list of kernels
# C - no. of clusters
def mkl_cluster(kernels, z, C=2, n_iter=1000):
    m = len(kernels)  # no. of kernels
    n = kernels[0].shape[0]  # no. of examples


    # init assignment matrix
    H = empty((n, C))

    for i in range(n_iter):
        print 'Iteration ',i
        # STEP 1: Find the largest eigenvalues
        combined_K = calc_comp_kernel(kernels,z)
        evals, evecs = eigh(combined_K, eigvals=(n - C, n - 1))
        H = evecs
        #the following line is only necessary if we need to find the clusters at every iteration
        H_norm = H / matlib.repmat(sqrt(sum(H**2,1)),C,1).T

        # STEP 2: QP
        coeff = [trace(K) - trace(dot(H.T, dot(K, H))) for K in kernels]
        # call the main function 
        try: 
            new_z = run_mosek(coeff, m)
        except mosek.Exception as e: 
            print ("ERROR: %s" % str(e.errno)) 
            if e.errno == 1295: #PSD error
                return False, "Not PSD Error"
            if e.msg is not None: 
                import traceback 
                traceback.print_exc() 
                print ("\t%s" % e.msg) 
            sys.exit(1) 
        except: 
            import traceback 
            traceback.print_exc() 
            sys.exit(1) 
        print ("Finished OK")

        # check for convergence
        if has_converged(z, new_z):
            print 'CONVERGED'
            z = new_z
            break
        else:
            z = new_z
    # run k-means
    theta_sum = int(sum(z))
    print 'theta_sum : ', theta_sum
    #if theta_sum < 1 :
    #   return False, "Theta did not sum to 1"
    #if C == 2:
    #    plt.scatter(H_norm[:,0],H_norm[:,1])
    #    plt.show()
    #if C == 3:
    #    fig = plt.figure()
    #    ax = fig.add_subplot(111, projection='3d')
    #    ax.scatter(H[:,0],H[:,1],H[:,2], c='r', marker='o')        
    #    plt.show()
    clustering_label = KMeans(n_clusters = C,max_iter=1000).fit_predict(H_norm)
    kmeans_obj_score = KMeans(n_clusters = C,max_iter=1000).fit(H_norm).inertia_
    print 'kmeans_obj_score',kmeans_obj_score
    return True, z, clustering_label,combined_K,kmeans_obj_score


def has_converged(z_old, z_new, tol=1e-5):
    conv = False
    if mean(abs(z_old - z_new)) < tol:
        conv = True
    return conv



def is_symmetric(arr):
    if all(arr == arr.T):
        return True
    else:
        return False


def calc_CER(result, solution):
    num_points = len(result)
    count_disagree = 0
    for i in range(num_points):
        for j in range(i + 1, num_points):
            if (result[i] == result[j] and solution[i] !=  solution[j]) or (result[i] != result[j] and solution[i] ==  solution[j]) :
                 count_disagree += 1
    print count_disagree
    return count_disagree / ((num_points * (num_points-1)) / 2.0)             

def calc_CER_matrix(result, solution):
    num_points = len(result)
    dist_matrix_1 = zeros((num_points, num_points))
    dist_matrix_2 = zeros((num_points, num_points))


    for i in range(num_points):
        for j in range(num_points):
            if result[i] == result[j]:
                dist_matrix_1[i, j] = 1 
            if solution[i] ==  solution[j]:
                dist_matrix_2[i, j] = 1 
  
    final_distance = sum(abs(dist_matrix_1 - dist_matrix_2))  / (num_points * num_points)
    return final_distance


# this is a recommendation from the Tzortsis et al paper (kernel-based weighted multi-view clustering)
# normalization of the kernels by average pairwise distance
def norm_avg_pairwise_dist(K):
    #print 'kernel within norm_avg_pairwise ', K
    #print np.count_nonzero(K)
    avg_pair_distance = 0
    (num_patients, num_patients) = K.shape
    for i in range(0, num_patients):
        for j in range(0, num_patients):
            avg_pair_distance = avg_pair_distance + (K[i,i] - 2 * K[i,j] + K[j,j])
    avg_pair_distance = avg_pair_distance / float(num_patients * num_patients)
    #print avg_pair_distance
    return K / float(avg_pair_distance)

# the following three functions are for sparsity: soft-thresholding
def l2norm(vec):
    return sqrt(sum(vec**2))

# soft thresholding function
def soft(data, value):
    return (sign(data) * maximum(0,abs(data) - value ) )

# choose delta > 0 such that ||w||_1 = s
def binarySearch(a, s):
    if l2norm(a) == 0 or sum(abs(a/l2norm(a))) <= s:
        return 0 # delta = 0 if it results in ||w||_1 <= s
    lam1 = 0
    lam2 =  amax(abs(a)) - 1e-5
    iter = 1
    while iter <= 15 and  (lam2-lam1)>(1e-4):
        su = soft(a,(lam1+lam2)/2)
        if(sum(abs(su/l2norm(su))) < s):
            lam2 = (lam1+lam2)/2
        else:
            lam1 = (lam1+lam2)/2

        iter = iter + 1
    return (lam1+lam2) / 2


def center(data):
    means = data.mean(axis=0)
    return data - means


def calc_wcss(x, Cs):
    #calculates the within cluster sum of squared distance for each feature separately
    num_points = x.shape[0]
    num_features = x.shape[1]
    wcss = zeros(num_features)
    for k in unique(Cs): #for each cluster
        indices = nonzero(Cs==k)[0]
        x_current = center(x[indices,:])
        wcss = wcss + sum(x_current ** 2, axis=0) # this calculation already handles the 1/n normalization

    x_center = center(x)
    # bcss is the between cluster sum of squares
    bcss =  sum(x_center ** 2, axis=0) - wcss #eq 8 of Tibshirani paper
    #print 'within calc wcss '
    #print sum(x_center ** 2, axis=0), wcss
    return (wcss, bcss)


def find_optimal_k_with_gap(data,num_perm, k_list):
    total_num_features = 0
    num_views = len(data)
    num_of_patientss = data[1].shape[0]
    permuted_data = [0] * num_views
    sd_k = [0] * len(k_list)
    sk = [0] * len(k_list)
    for v in range(len(data)):
        total_num_features +=data[v].shape[1]
        permuted_data[v] = array(zeros(data[v].shape))
    obj_fun_perm_list = [] #[0] * num_perm    
    gap  = [0] * len(k_list)
    n = data[0].shape[0]
    K = zeros((num_views, n, n))
    for v in range(num_views):
        a = kernel(data[v],g=2**-16)
        #a = linear_kernel(data[v])
        #a = around(a,8)
        #a = a - amin(a)        
        #print 'Original', a[0:5,0:5]
        K[v] = a
    z = ones(num_views) * 1./ num_views
    for i in range(len(k_list)):
        num_clusters = k_list[i]
        # run mkl_cluster on the original data
        mkl_result = mkl_cluster(K,z, num_clusters,10)
        if(mkl_result[0]):
            theta_orig = mkl_result[1]
            C_orig = mkl_result[2]
            composite_kernel_orig = mkl_result[3]
            obj_fun_orig = mkl_result[4]
        else :
            continue
        
        print 'original dataset is done'
        
        for iter in range(num_perm):

            for v in range(num_views):
                for j in range(data[v].shape[1]):
                    permuted_data[v][:,j] = random.uniform(min(data[v][:,j]),max(data[v][:,j]),num_of_patientss) #permutation(data[v][:,j])
            K_perm = zeros((num_views, n, n))
            for v in range(num_views):
                num_gene_features = data[v].shape[1]
                gamma = 1 / (2.0 * num_gene_features * num_gene_features)
                a = kernel(permuted_data[v],g=2**-16)
                #a = linear_kernel(permuted_data[v])
                #a = around(a,8)
                #a = a - amin(a)                
                print a[0:5,0:5]
                K_perm[v] = a            
            perm_result = mkl_cluster(K_perm,z, num_clusters,10)
            if(perm_result[0]):
                theta = perm_result[1]
                C = perm_result[2]
                composite_kernel = perm_result[3]
                obj_fun = perm_result[4]
                obj_fun_perm_list.append(obj_fun)
            else :
                continue            
        
        gap[i]  = (1.0/num_perm) * mean(obj_fun_perm_list) - math.log(obj_fun_orig)
        l = (1.0/num_perm) * mean(obj_fun_perm_list)
        obj_func_diff = 0
        for obj_l in range(num_perm):
            obj_func_diff += pow((obj_fun_perm_list[obj_l] - l),2)
        sd_k[i] = pow(((1.0/num_perm) * (obj_func_diff)),1.0/2)
        sk[i] = sd_k[i] * sqrt(1+1.0/num_perm)
        
    optima_k_list = []
    for k_cand in range(len(k_list)-1):
        if k_cand == len(k_list):
            continue
        gap_diff = gap[k_cand+1] - sk[k_cand +1]
        if gap[k_cand] >= gap_diff:
            optima_k_list.append(k_list[k_cand])
    k_optima = min(optima_k_list)
    print 'Gap statistics ' , gap
    print 'Standard Deviation ', sd_k
    return k_optima,gap,sd_k



def has_converged_fs(w_old, w_new, tol=1e-4):
    conv = False
    if (sum(abs(w_old - w_new))) / sum(abs(w_old)) < tol:
        conv = True
    return conv

def mkl_feature_selection(data, num_clusters, s):
      
    num_views = len(data) 
    mvkkm_p = 1.5
    p = [0] * num_views
    n = 0
    w = [0] * num_views
    # init view weights
    theta = ones(num_views) * 1./ num_views    # Shankar calls this z
    n = data[0].shape[0]  # of data points
    #initialize w
    #print 'shape ', data[0].shape
    total_num_features = 0
    for v in range(num_views):
        p[v] = data[v].shape[1]   # of dimensions
        w[v] = ones(p[v]) * 1 / sqrt(p[v]) # initialization recommended by Tibshirani paper
        total_num_features += p[v]
    
    #print 'total number of features is ', total_num_features
    #print 'p is ', p
    #s = sqrt(total_num_features)
    #s = 1.5
    #s = 5
    obj_fun = 0
    #iterate until convergence
    sol_status = True
    C_old = zeros(n)
    #fhdebug = open('debug.txt', 'w')
    for iter in trange(0,30):
        #print 'FEATURE SELECTION iteration ', iter, 's is ', s
        # PART 1, w is fixed learn theta and C 
        scaled_data_view = [0] * num_views
        K = zeros((num_views, n, n))
        K_concat = []
        terminate_early = False
        #calculate kernel with scaled features
        for v in range(num_views):
            if np.count_nonzero(w[v]) == 0:
                w[v][0] = 0.0001

            scaled_data_view[v] = data[v] * w[v]   # this scales each feature column with the corresponding weight for that feature
            #print 'view ', v, 'number of nonzero ', np.count_nonzero(w[v])
            #print >>fhdebug, 'view ', v, 'number of nonzero ', np.count_nonzero(w[v])
            #fhdebug.flush()
            scaled_data = scale(scaled_data_view[v]) # standardization
            a = linear_kernel(scaled_data)

            if np.count_nonzero(w[v]) > 0:
                a = norm_avg_pairwise_dist(a)
            else:

                terminate_early = True
                sol_status = 'early_termination all zero'
                print 'Early termination'


            a = around(a,8)
            a = a - amin(a)
 
            #print 'symmetric ', is_symmetric(a)
            K[v] = a
            K_concat.extend(a)
        if terminate_early:
            break
        #run MKL
        #input prev theta to MK
        # get updated theta and clustering assignments
        #theta, C, composite_kernel, 
        #cluster_init_assign = [random.randint(0,num_clusters) for r in xrange(a.shape[0])]
        if iter == 0:
            #random.seed(2)  
            #cluster_filename = '/Users/User/Dropbox/Tunde/rMKL_DR/new_data/Lung/results_15k/clustering_label.txt'
            
            '''
            cluster_filename = '/home/mllab/Dropbox/Tunde/analysis/final_results_v2/Tunde_thesis_feature_selection_results/Breast__k_3_s_28_run_1_init_thesis_rbf__clustering.txt'
            fhinit = open(cluster_filename)
            cluster_init_assign = []
            for line in fhinit:
                 cluster_init_assign.append(int(line.split()[0]))
            '''
            
            cluster_init_assign = [random.randint(1,num_clusters+1) for r in xrange(a.shape[0])]
            C_old = np.array(cluster_init_assign)
            #print 'cluster init assign ', cluster_init_assign
            cluster_init_assign_iter0 = cluster_init_assign
            #cluster_init_assign = loadtxt('initial_cluster_assignment.txt')
        else:
            cluster_init_assign = C
        K_concat = array(K_concat)
        #print K_concat.shape
        #print 'Theta before : ', theta
        
        #print 'mkl is starting'
        mkl_result = MVKKM(K_concat, num_clusters, num_views, mvkkm_p, cluster_init_assign, theta)
        # print 'w', mkl_result[1]
        # print mkl_result[2]
        # print mkl_result[3]
        # print mkl_result[4]
        #print 'mkl is done'
        #mkl_result = mkl_cluster(K,theta, num_clusters,10)
        if(mkl_result[0]):
            theta = mkl_result[1]
            C = mkl_result[2]

            # print 'clustering result at iteration ', iter, ' is ', C
            # print 'clustering difference with prev iter ', np.sum(C_old == C), 'out of ', n
            # print 'View weights ', theta
            composite_kernel = mkl_result[3]
            kmeans_obj_funct = mkl_result[4]
            C_old = C[:]
        else :
            sol_status = False
            print 'MKL MSG : ', mkl_result[1]
            return (sol_status, mkl_result[1])

        #print 'Theta After : ', theta
        #print C
        # a concatenates all the feature vectors
        a = [0] * total_num_features
        spos = 0
        bcss_tuple = [0] * num_views
        wcss_tuple = [0] * num_views
        # PART 2, C and theta is fixed learn w
        for v in range(num_views):
            wcss_tuple[v], bcss_tuple[v] = calc_wcss(data[v], C)
            #print '1134 ', spos, spos + p[v], len(bcss_tuple[v]), len(theta[v])
            #print 'view v bcss ', bcss_tuple[v][1:5]
            a[spos:spos + p[v]] = bcss_tuple[v] * theta[v]
            spos += p[v]
        a = array(a)
        #print 'a is ', a[0:10], 's is ', s
        lam = binarySearch(a,s)
        #print 'lam is ',lam
        w_unscaled = soft(a, lam)
        if iter > 0:
            w_old = w_scaled # previous iteration's w

        w_scaled =  w_unscaled / l2norm(w_unscaled)

        #print 'Before', w[2].shape
        spos = 0
        for v in range(num_views):
            w[v] = w_scaled[spos:spos + p[v]]
            spos += p[v]

        obj_fun = 0
        #print bcss_tuple[0].shape
        #print w[0].shape
        for v in range(num_views):	  
            obj_fun += sum((wcss_tuple[v] * w[v])* theta[v])
            #print ' line 1330 view ', v, 'number of nonzero ', np.count_nonzero(w[v])
            #print 'view ', v, list(w[v])

        if iter > 0 and has_converged_fs(w_old, w_scaled):
            #print 'Feature selection has converged'
            break


    return (sol_status, w_scaled, theta, C, composite_kernel, obj_fun,kmeans_obj_funct, w, cluster_init_assign_iter0)


def mkl_feature_selection_with_leave_one_out_cv(data, num_clusters, s):
    full_set_cluster_assignment = mkl_feature_selection(data, num_clusters, s)[3]
    savetxt('../final_results_v2/leave_one_out_complete_patient_initial_run_cluster_label_s_' + str(s) + '.txt',full_set_cluster_assignment ,fmt="%d")
    C_matrix = []
    num_of_patients = data[0].shape[0]
    print num_of_patients
    print data[0].shape
    print data[1].shape
    print data[2].shape
    
    num_views = len(data) 
    data_all = [data[0],data[1],data[2]]
    for patient in range(num_of_patients):
        print 'RUNNING FOR PATIENTS ', patient, ' OUT OF ', num_of_patients
        for v in range(num_views):
            data[v] = delete(data_all[v], patient, 0)
            print data[v].shape
        mvkkm_p = 1.5
        p = [0] * num_views
        n = 0
        w = [0] * num_views
        # init view weights
        theta = ones(num_views) * 1./ num_views    # Shankar calls this z
        n = data[0].shape[0]  # of data points
        #initialize w
        #print 'shape ', data[0].shape
        total_num_features = 0
        for v in range(num_views):
            p[v] = data[v].shape[1]   # of dimensions
            w[v] = ones(p[v]) * 1 / sqrt(p[v]) # initialization recommended by Tibshirani paper
            total_num_features += p[v]
    
        print 'p is ', p
    
        obj_fun = 0
        #iterate until convergence
        sol_status = True
        for iter in range(0,5):
            # PART 1, w is fixed learn theta and C 
            scaled_data_view = [0] * num_views
            K = zeros((num_views, n, n))
            K_concat = []
            #calculate kernel with scaled features       
            for v in range(num_views):
                scaled_data_view[v] = data[v] * w[v]   # this scales each feature column with the corresponding weight for that feature
                scaled_data = scale(scaled_data_view[v]) # standardization
                a = linear_kernel(scaled_data)
                a = norm_avg_pairwise_dist(a)
                print 'a linear  ',v
                print a[0:5,0:5]
                a = around(a,8)
                a = a - amin(a)
                #
                print 'symmetric ', is_symmetric(a)
                K[v] = a
                K_concat.extend(a)
    
            if iter == 0:
                cluster_init_assign = [random.randint(1,num_clusters+1) for r in xrange(a.shape[0])]
    
            else:
                cluster_init_assign = C
            K_concat = array(K_concat)
            print K_concat.shape
            print 'Theta before : ', theta
            
            mkl_result = MVKKM(K_concat, num_clusters, num_views, mvkkm_p, cluster_init_assign, theta)
            #mkl_result = mkl_cluster(K,theta, num_clusters,10)
            if(mkl_result[0]):
                theta = mkl_result[1]
                C = mkl_result[2]
                composite_kernel = mkl_result[3]
                kmeans_obj_funct = mkl_result[4]
            else :
                sol_status = False
                print 'MKL MSG : ', mkl_result[1]
                return (sol_status, mkl_result[1])
            
            print 'Theta After : ', theta
            print C
            # a concatenates all the feature vectors
            a = [0] * total_num_features
            spos = 0
            bcss_tuple = [0] * num_views 
            # PART 2, C and theta is fixed learn w
            for v in range(num_views):
                wcss, bcss_tuple[v] = calc_wcss(data[v], C)
                a[spos:spos + p[v]] = bcss_tuple[v] * theta[v]
                spos += p[v]
            a = array(a)
            lam = binarySearch(a,s)
    
            w_unscaled = soft(a, lam)
            if iter > 0:
                w_old = w_scaled
            w_scaled =  w_unscaled / l2norm(w_unscaled)
    
            #print 'Before', w[2].shape
            spos = 0
            for v in range(num_views):
                w[v] = w_scaled[spos:spos + p[v]]
                spos += p[v]
    
            obj_fun = 0
            print bcss_tuple[0].shape
            print w[0].shape
            for v in range(num_views):	  
                obj_fun += sum((bcss_tuple[v] * w[v])* theta[v])
            print obj_fun
            print 'w ', w
            print 'Theta ', theta
            if iter > 0 and has_converged_fs(w_old, w_scaled) or iter == 4:
                k_clust_error = []
                unique_C_list = unique(C)
                for k in unique_C_list: #for each cluster
                    indices = nonzero(C==k)[0]
                    v_clust_error = 0
                    for v in range(num_views):
                        x_current = center(data[v][indices,:])                
                        v_C_dist = sum(((data_all[v][patient,:] * w[v] * theta[v]) - (x_current * w[v] * theta[v]))**2)
                        v_clust_error += v_C_dist
                    k_clust_error.append(v_clust_error)
                patient_clust_assgn = unique_C_list[k_clust_error.index(min(k_clust_error))]
                # create the complete cluster assignment
                C = list(C)
                C.insert(patient,patient_clust_assgn)
                C_matrix.append(C)
                # evaluate base on rand index etc
                break
    savetxt('../final_results_v2/leave_one_out_final_cluster_label_s_' + str(s) + '.txt',C_matrix,fmt="%d")
    return (sol_status, w_scaled, theta, C, composite_kernel, obj_fun,kmeans_obj_funct)



def assign_missing_patients(bootstrapped_model_result, bootstrapped_data, data_all, missing_patient_indices, num_views, only_missing = False):
    C = bootstrapped_model_result[3]
    if  only_missing:
        C_full = np.array([]) 
    else:
        C_full = C[:]
    
    theta = bootstrapped_model_result[2]
    w = bootstrapped_model_result[7]
    print 'number of missing patients ', len(missing_patient_indices)
    for patient in missing_patient_indices: 
        #print 'assign missing patients ', patient
        k_clust_error = []
        unique_C_list = unique(C)
        for k in unique_C_list: #for each cluster
           indices = nonzero(C==k)[0]
           v_clust_error = 0
           
           for v in range(num_views):
               x_current = bootstrapped_data[v][indices,:].mean(axis = 0) # calculate column means
               #x_current = center(bootstrapped_data[v][indices,:])         
               #savetxt('x_current_' + str(v) + '.txt', x_current)
               #savetxt('patient' + str(v) + '.txt', data_all[v][patient,:])
               
               #print 'view ', v, ' x_current ', x_current, ' x current shape ', x_current.shape
               a = data_all[v][patient,:] * w[v] * theta[v]
               b = (x_current * w[v] * theta[v])
               #print a.shape
               #print b.shape
               c = a - b
               #print c.shape
               v_C_dist = sum(((data_all[v][patient,:] * w[v] * theta[v]) - (x_current * w[v] * theta[v]))**2)
               v_clust_error += v_C_dist
               #print 'view  ', v, ' distance ', v_C_dist
           k_clust_error.append(v_clust_error)
        #print 'k clust error ', k_clust_error   
        patient_clust_assignment = unique_C_list[k_clust_error.index(min(k_clust_error))]
        #print 'C_full size before ', C_full.size 
        C_full = np.append(C_full, patient_clust_assignment)
        #print 'C_full size ', C_full.size 
    return C_full		



def load_arr(fname):
    X =  []
    data = []
    fh = open(fname)
    l = [ map(float,line.split(',')) for line in fh ]
    return l
    
    
def load_shamir(fname, header = True):

    X =  []
    data = []
    fh = open(fname)
    header = fh.readline()
    patient_ids = header.split()
    for ctr,line in enumerate(fh.readlines()):
        #if header == True and ctr==0: continue # exclude header
        dat = array(line.strip().strip('\n').split()[1::], float)
        data.append(dat)


    data = array(data)
    #print (data.shape)

    X.extend(data)
    X = array(X).T

    return [X, patient_ids]

def load(fname, cancer_type, data_type, header = True):

    X =  []
    data = []
    fh = open(fname)
    counter = 0
    for ctr,line in enumerate(fh.readlines()):
        if header == True and ctr==0: continue # exclude header

        if cancer_type == 'simulation':
            dat = array(line.strip().strip('\n').split(), float) # start from the very first column 
            #print dat.size
        elif cancer_type == 'Kidney' and data_type == 'Methy':
            dat = array(line.strip().strip('\n').split()[2::], float)
            if counter == 0:
                print dat
        else:
            dat = array(line.strip().strip('\n').split()[1::], float)
        data.append(dat)
        counter += 1

    data = array(data)
    #print (data.shape)

    X.extend(data)
    if cancer_type != 'simulation':
         X = array(X).T

    return X

def weighted_kernel_kmeans(cluster_assign, K, dataset_weights, num_clusters):
    e = 0.0001
    clustering_error = 0
    num_patients = K.shape[1] 
    dist_center = zeros((num_patients,1))
    iter = 1
    #print 'cluster assignment ', cluster_assign
    #print dataset_weights
    while True:
        old_clustering_error = clustering_error
        clustering_error = 0
        intra_distance = zeros((num_clusters,1))
        dist_cluster = zeros((num_patients, num_clusters))
        for i in range(0, num_clusters):
            #find the data points that belong to cluster i
            curr_cluster_points = where(array(cluster_assign) == (i+1))[0]
            cluster_size = curr_cluster_points.shape[0]
            #print 'Cluster size for i is : ', i, cluster_size 
            #print  'curr_cluster_points ', curr_cluster_points
            curr_cluster_weights = dataset_weights[curr_cluster_points]
            # calculate intra_cluster pairwise quantity for cluster i
            K_sub = K[ix_(curr_cluster_points, curr_cluster_points)]    
            intra_distance[i] = dot(transpose(dot(K_sub,curr_cluster_weights)), curr_cluster_weights)
            intra_distance[i] = intra_distance[i] / pow(sum(curr_cluster_weights),2)
            # calculate point-cluster quantity between all points and cluster i
            dist_cluster[:,i] = dot(K[:, curr_cluster_points],curr_cluster_weights).squeeze()
            # calculate the distance of all points to the center of cluster i 
            dist_cluster[:,i] = (-2 * dist_cluster[:,i] / sum(curr_cluster_weights)) + intra_distance[i] + diag(K)     
            #store the distance of cluster's i points to their cluster center
            dist_center[curr_cluster_points] = dist_cluster[curr_cluster_points,i].reshape((cluster_size,1))
            #add the contribution of cluster i to the clustering error
            clustering_error = clustering_error + dot(transpose(curr_cluster_weights),dist_cluster[curr_cluster_points, i])
            #print 'cluster i and clustering error ', i, clustering_error
        update_cluster_assign = argmin(dist_cluster,axis = 1) + 1
        #print 'Kernel K-means iteration ' , iter, ' clustering error ', clustering_error
        #print 'dist_cluster ', dist_cluster
        if iter > 1:
            if abs(1.0 - (clustering_error / old_clustering_error)) < e:
                break
        if iter >= 10:
            break

        cluster_assign = update_cluster_assign	    
        # drop empty clusters
        count = 0
        for i in range(1, num_clusters+1):
            if size(where(update_cluster_assign == i)[0]) == 0:
                tmp = where(update_cluster_assign > i)[0]
                cluster_assign[tmp] = cluster_assign[tmp] - 1
                count = count + 1 
        num_clusters = num_clusters - count
        iter = iter + 1
    return (cluster_assign,clustering_error,dist_center)	       


def MVKKM(K, num_clusters, num_views, p, cluster_init_assign, w_init):
    num_data_per_view = K.shape[0] / num_views # num_rows / num_views
    #print 'num_data_per_view ', num_data_per_view
    e = 0.000001
    trace_per_kernel_1 = zeros((num_views,1))
    for v in range(0, num_views):
        trace_per_kernel_1[v] = trace(  K[v*num_data_per_view: (v + 1) * num_data_per_view,:])
        #print 'trace  ', trace(  K[v*num_data_per_view: (v + 1) * num_data_per_view,:])
        #print 'trace per kernel ', trace_per_kernel_1[v]                  

    iter = 1
    old_clustering_error = float("inf")
    cluster_assign = cluster_init_assign
    w = w_init
    #print 'w is ', w 
    #K = around(K,4)
    while iter < 10:
        # print 'Iteration ' , iter
        # print 'Updating the clusters for given weights'
        # calculate the composite kernel
        K_comp = zeros((num_data_per_view, num_data_per_view))
        for i  in range(0, num_views):
            K_view = K[i*num_data_per_view: (i + 1) * num_data_per_view,:]	    
            #print  'w is', pow(w[i],p)	, K_view[5,6]   
            #print 'w is ', w
            #print 'w[i] is', w[i], p, pow(w[i],p)
            #print round(pow(w[i],p),4)
            K_comp = K_comp + round(pow(w[i],p),4) * K[i*num_data_per_view: (i + 1) * num_data_per_view,:]
            #print 'K comp ',K_comp[5,6]
        #print 'K_comp ', K_comp[0:4,0:4]    
        # run kernel k-means to get new assignment
        (cluster_assign, clustering_error, dist_center) = weighted_kernel_kmeans(cluster_assign, K_comp, ones((num_data_per_view,1)), num_clusters)
        #print  'iter ', iter, list(cluster_assign)  #, clustering_error, dist_center
        iter += 1

        if len(unique(cluster_assign)) < num_clusters:
            print 'ERROR empty cluster detected'
            #exit()

        # calculate the cluster indicator matrix Y (see (3) in the paper)
        #Y = csc_matrix(([num_data_per_view-1,0],([num_clusters,1],[1,num_data_per_view-1])), dtype = float)
        Y = lil_matrix((num_data_per_view,(num_clusters+1)), dtype = float)
        for i in range(1, num_clusters+1):
            point_cluster_i = where(array(cluster_assign) == i)
            cluster_i_size = point_cluster_i[0].shape[0]
            Y[point_cluster_i,i] = 1/sqrt(cluster_i_size)
        #print Y
        #store the new trace value tr(Y' K_v Y) for each kernel k_v after kernel kmeans run.
        trace_per_kernel = zeros((num_views,1))
        for v in range(0, num_views):
            trace_per_kernel[v] = trace(Y.T * K[v*num_data_per_view: (v + 1) * num_data_per_view,:] * Y )
        #print trace_per_kernel
        #print trace_per_kernel.shape
        #exit()

        # store the per view trace diff
        # these are per view intra-cluster variance after updating the cluster (D_v quantity in the paper)
        trace_per_kernel_diff = trace_per_kernel_1 - trace_per_kernel
        #print 'trace_per_kernel_diff ', trace_per_kernel_diff 

        # check for convergence
        if abs(1-(clustering_error/old_clustering_error)) < e:
            #print('MVKKM reached convergence')
            break
        old_clustering_error = clustering_error


        # second part. updating the weight for given clusters.
        #print 'updating the weight'
        #update the weight using close form expression (see (9) and (10) in the paper)
        if p != 1:
            #print (power(trace_per_kernel_diff,(1/(p-1)) ))
            w = divide(1,(multiply((power(trace_per_kernel_diff,(1/(p-1))) ), sum( 1. / power(trace_per_kernel_diff,(1/(p-1)))))).T)
            #print 'w at 1487 ', w
            w = w[0]
        else:
            min_tr = min(trace_per_kernel_diff)
            min_tr_pos = trace_per_kernel_diff.index(min_tr)
            w = zeros((num_views,1))
            w = (min_tr_pos) = 1

        #tiny weight values are set to zero for better stability
        tiny_index = where(array(w)< pow(10,-5))[0]
        w[tiny_index] = 0;
        w = w/sum(w);

    return True, w, cluster_assign,K_comp,clustering_error


def generate_regkmeans_pg157_data(n, p, num_redundant_features, mu):
    cluster_size = n / 4
    solution = np.concatenate(
        (np.ones(cluster_size), np.ones(cluster_size) * 2, np.ones(cluster_size) * 3, np.ones(cluster_size) * 4),
        axis=0)

    identity50 = np.identity(50)
    ones25 = np.ones(25)
    ones50 = np.ones(50)

    cluster1_means = np.concatenate((-1 * ones25 * mu, ones25 * mu))
    cluster2_means = mu * ones50
    cluster3_means = np.concatenate((ones25 * mu, -1 * ones25 * mu))
    cluster4_means = -1 * mu * ones50
    cluster1 = multivariate_normal(cluster1_means, identity50, size=cluster_size)
    cluster2 = multivariate_normal(cluster2_means, identity50, size=cluster_size)
    cluster3 = multivariate_normal(cluster3_means, identity50, size=cluster_size)
    cluster4 = multivariate_normal(cluster4_means, identity50, size=cluster_size)

    data = np.concatenate((cluster1, cluster2, cluster3, cluster4), axis=0)  # concatenate the rows
    for i in range(num_redundant_features):
        redundant_feature = np.random.normal(0, 1, size=n)
        redundant_feature = reshape(redundant_feature, (-1, 1))
        data = np.concatenate((data, redundant_feature), axis=1)
    print data.shape

    mu = 0.4
    cluster1_means = np.concatenate((-1 * ones25 * mu, ones25 * mu))
    cluster2_means = mu * ones50
    cluster3_means = np.concatenate((ones25 * mu, -1 * ones25 * mu))
    cluster4_means = -1 * mu * ones50
    cluster1 = multivariate_normal(cluster1_means, identity50, size=cluster_size)
    cluster2 = multivariate_normal(cluster2_means, identity50, size=cluster_size)
    cluster3 = multivariate_normal(cluster3_means, identity50, size=cluster_size)
    cluster4 = multivariate_normal(cluster4_means, identity50, size=cluster_size)

    noisy_view = np.concatenate((cluster1, cluster2, cluster3, cluster4), axis=0)  # concatenate the rows
    for i in range(num_redundant_features):
        redundant_feature = np.random.normal(0, 1, size=n)
        redundant_feature = reshape(redundant_feature, (-1, 1))
        noisy_view = np.concatenate((noisy_view, redundant_feature), axis=1)

    uninfor_means = np.ones(50)
    uninforview = multivariate_normal(uninfor_means, np.identity(50), size=n)

    return [[data, uninforview], solution]