import pandas as pd

infile = '../new_datasets/OV/methy_orig.txt'
outfile = '../new_datasets/OV/methy.txt'

df = pd.read_table(infile, sep=' ', header = 0, index_col=0)

df.columns = df.columns.str.replace('-','.')

df.to_csv(outfile, sep=' ')