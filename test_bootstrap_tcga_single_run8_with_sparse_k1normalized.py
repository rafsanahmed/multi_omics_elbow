﻿import numpy as np
from scipy.stats import multivariate_normal
from util import *
from tqdm import tqdm,trange
import glob

from featureselect_tuning_code import *
    

'''
import pandas as pd

#get_ipython().magic('matplotlib inline')
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm

font = fm.FontProperties(size=25)

#larger
from pylab import rcParams
rcParams['figure.figsize'] = 15, 10

from lifelines.estimation import KaplanMeierFitter
from lifelines import CoxPHFitter
from lifelines.statistics import multivariate_logrank_test
'''
def center(data):
    means = data.mean(axis=0)
    return data - means

if __name__ == "__main__":


    cancer_type = 'LUSC'


    print cancer_type
    Gene_data = array(load('../new_datasets/' + cancer_type + '/' + cancer_type + '_gene_subset.txt', cancer_type, 'gene'))
    Methy_data = array(load('../new_datasets/' + cancer_type + '/' + cancer_type +  '_methylation_subset.txt',cancer_type,'mirna'))
    Mirna_data = array(load('../new_datasets/' + cancer_type + '/' + cancer_type +  '_mirna_subset.txt',cancer_type, 'mirna'))

    print Gene_data.shape
    print Methy_data.shape
    print Mirna_data.shape
    
    A = norm(Gene_data, 'standard')
    B = norm(Methy_data, 'standard')
    C = norm(Mirna_data, 'standard')
    data_tuple = [A,B,C]


    # Colon s = 42, k = 3
    # Kidney s = 38, k = 6
    single_run = True
    survival = False
    k_original = 8
    runs = 100
    sparsity = 60
    sparsity_range = range(20,sparsity+1,5)
    mainpath = '../final_results_v2/tcga/' + cancer_type +'_k' + str(k_original) + '_s' + str(sparsity) +  '_clustering_results_single_run_4/'
    if not os.path.exists(mainpath):
        os.makedirs(mainpath)

    maxfile = open(mainpath+ cancer_type+'_maxfile.txt','w')
    if single_run == True:
      pvalues = []   
      for s in sparsity_range:
        
       for run in trange(1,runs+1+100):
        print 'run', run
        try:
            obj_function_dict = {}
            for kval in trange(2,k_original+1):
                # r specifies the number of redundant features
                k = kval # known clusters
                result = mkl_feature_selection(data_tuple, k, s)
                #file_key = '../final_results_v2/tcga/clustering_results_single_run/'+ cancer_type +'_' +  '_k_' + str(k) + '_s_' + str(s) + '_run_' + str(run)
                #file_key = '../final_results_v2/tcga/clustering_results_single_run_8/'+ cancer_type  +  '_k_' + str(k) + '_s_' + str(s) + '_run_' + str(run)

                #fhout = open(file_key + '.txt', 'w')
                #cluster_filename =  file_key +  '_clustering.txt'
                #fhout_clustering = open(cluster_filename, 'w')

                cluster_init_assignment = result[8]
                #init_filename =  file_key + '_init.txt'
                #fhinit = open(init_filename, 'w')
                #print >>fhinit, '\t'.join(   [str(elem) for elem in list(cluster_init_assignment)])

                #kmeans = KMeans(n_clusters=2, random_state=0).fit(View1_data)
                #print kmeans.labels_
                #result = list(kmeans.labels_)
                #print result[0:50].count(0), result[0:50].count(1)
                #print result[50:100].count(0), result[50:100].count(1)

                #print >>fhout, 'Solution status : ',result[0]
                #print >>fhout, 'View weights : ',list(result[2])
                #for v in range(len(result[7])):
                    #print >>fhout, 'View ', v, 'Feature weights : ',  list(result[7][v])


                #obj function matlab (as given) save in an array

                #print >>fhout, 'Cluster assignment: ', result[3]
                #print >>fhout, 'Obj Function : ', result[5]
                #for elem in result[3]:
                #    print >>fhout_clustering, elem
                #fhout_clustering.close()


                obj_function_dict[k]=result[5]
                if k == 2:
                    num_views = len(data_tuple)
                    print ('Use w for 1 cluster')
                    w_2 = result[7]
                    c_2 = ones(len(result[3]))
                    #print num_views
                    obj_function = 0
                    theta = ones(num_views) * 1. / num_views
                    bcss_tuple = [0] * num_views
                    wcss_tuple = [0] * num_views
                    for i in range(num_views):
                        wcss_tuple[i], bcss_tuple[i] = calc_wcss(data_tuple[i], c_2)

                    for i in range(num_views):
                        ww = (float(sum(w_2[i]))/ float(len(w_2[i])))*ones(len(w_2[i]))
                        #print 'ww',len(ww), len(w_2[i])
                        obj_function += sum((wcss_tuple[i] * ww)* theta[i])
                    obj_function_dict[1] = obj_function


                #print calc_CER(result[3], solution)
                #print calc_CER_matrix(result[3], solution)
                # run survival analysis
                if survival:
                    clinical_filename = '../new_datasets/' + cancer_type + '/' + cancer_type +  '_clinical_data.txt'
                    #drug_filename = '../new_datasets/' + cancer_type + '/' + cancer_type +  '_drug_data.txt'
                    #cluster_label = '../final_results_v2/tcga/clustering_results_single_run/' + cancer_type +'_' +  '_k_' + str(k) + '_s_' + str(s) + '_clustering.txt'
                    #pam50_file = '/home/mllab/Dropbox/Tunde/analysis/PAM50/bioclassifier_R/Breastcancerpan50subtyperesult_pam50scores.txt'
                    #rand_run = 5
                    #cluster_num = 4
                    #cluster_filename = '../new_results/MVKKM_newdata/labels/' + cancer_type + '_mvkkm_labels_K' +  str(cluster_num) + '_random_init_run' + str(rand_run) + '_rbf_kernel_old_gamma_with_shankar_distance_second_derv.txt'
                    #print cluster_label

                    '''
                    cluster_l = open(cluster_label).readlines()
                    cluster_list = np.array([s.rstrip() for s in cluster_l])
                    df = pd.read_csv(clinical_filename, delimiter='\t')
                    df['group'] = cluster_list
                    df.head()
                    kmf = KaplanMeierFitter()
        
                    results = multivariate_logrank_test(np.array(df['days_to_last_followup']),np.array(df['group']), np.array(df['event']) )
                    results.print_summary()
                    print results.p_value
                    '''
                    #commented
                    '''
                    #cluster_filename = '/Users/User/Dropbox/Tunde/rMKL_DR/new_data/Lung/results_15k/clustering_label.txt'
                    pvalue_filename  = file_key + '_pvalue_run_' + str(run) + '.txt'
                    plot_filename = file_key + '_plot_run_' + str(run) + '.pdf'
                    print '/usr/bin/Rscript run_survival_simple.R ' +  cluster_filename + ' ' +  clinical_filename  + ' ' + pvalue_filename + ' ' + plot_filename
                    #os.system('/usr/bin/Rscript run_survival_simple.R ' +  cluster_filename + ' ' +  clinical_filename  + ' ' + pvalue_filename + ' ' + plot_filename)
    
                    #following line commented out bcz 1 cluster error
                    os.system('Rscript run_survival_simple.R ' +  cluster_filename + ' ' +  clinical_filename  + ' ' + pvalue_filename + ' ' + plot_filename)
                    fpval = open(pvalue_filename)
                    pval = fpval.readline().split()[0]
                    pvalues.append(pval)'''
        except:
            print ('Run ' + str(run) + ' failed!')
            continue
        # print pvalues
        print 'obj_function_dict_length: ', len(obj_function_dict)
        second_derivative = {}
        for k in range(2, k_original):
            second_derivative[k] = obj_function_dict[k - 1] + obj_function_dict[k + 1] - 2 * obj_function_dict[k]
        # print second_derivative
        k_max = max(second_derivative, key=second_derivative.get)
        v_max = max(second_derivative.values())
        maxfile.write(str(run) + '\t' + str(k_max) + '\t' + str(v_max) + '\n')
        maxfile.flush()
        with open(mainpath +'second_derivative_wcss_k' + str(k_original)+\
                          '_s' + str(s)+ '_' + str(run) +'.txt', 'w') as of:
            for i in range(1, k_original + 1):
                of.write('k' + str(i) + '\t')
                try:
                    of.write(str(obj_function_dict[i]) + '\t')
                except:
                    of.write('\t')
                try:
                    of.write(str(second_derivative[i]) + '\t')
                except:
                    of.write('\t')
                of.write('\n')
        file_list = glob.glob(mainpath + 'second_derivative_k' + str(k_original) + '_s' + str(s)+'*.txt')
        print '# of files generated',len(file_list)
        if len(file_list) >= runs:
            exit()

    else:
        k_list = [2, 3, 4, 5, 6, 7, 8]
        num_of_sample = A.shape[0]
        start_position = 38
        end_position = 50
        step_size = 2
        s_list = range(start_position, end_position, step_size)

        bootstrap_start = 0
        bootstrap_end = 10 # not inclusive


        file_key = ''
        for k in k_list:
           file_key += str(k)
        

        file1 = '../final_results_v2/tcga/'+ cancer_type +'_best_s_common_btsrp_' + str(bootstrap_start) + '_to_' + str(bootstrap_end) + '_k_' + file_key + '.txt'
        file2 = '../final_results_v2/tcga/'+ cancer_type +'_distances_common_btsrp_' + str(bootstrap_start) + '_to_' + str(bootstrap_end) +  '_k_' + file_key + '.txt'
        select_tuning_param_with_bootstrap_common_same_btsrp(cancer_type, file1, file2, data_tuple, k_list, s_list, bootstrap_start, bootstrap_end)
       
    


    
# generate_regkmeans_pg157_data works fine when mu = 2, when mu = 0.8 it doesn't work, the second view was a copy of the first view, k = 4, s = 10
# when the second view is redundant view, mu = 2, works fine completely, the view weights, the feature weights, and the clustering is ok.


